package com.firstTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Метод sum декомпозирует работу путем запуска задач ColumnSummator, которые
 * реализуют интерфейс Runnable
 *
 * Для сихронизации потоков использовать wait - notify
 */
public class MatrixService {

    private static final AtomicInteger count = new AtomicInteger(0);
    private static final List<Integer> resultList = new ArrayList<>();

    public int sum(int[][] matrix, int nThreads){

        List<Integer> numbers = Arrays.stream(matrix)
                .flatMap(x -> Arrays.stream(x).boxed())
                .collect(Collectors.toList());

        int len = numbers.size();

        if (len % nThreads == 0){
            int rowsForThread = len / nThreads;

            for (int i = 0; i < len; i += rowsForThread){
                new Thread(new ColumnSummator(numbers.subList(i, i + rowsForThread))).start();
            }
        }
        else {
            int startThreadsRows = len / nThreads;
            int lastThreadRows = startThreadsRows + len % nThreads;

            for (int i = 0; i < len - lastThreadRows; i += startThreadsRows){
                new Thread(new ColumnSummator(numbers.subList(i, i + startThreadsRows))).start();
            }
            new Thread(new ColumnSummator(numbers.subList(len - lastThreadRows, len))).start();
        }

        System.out.println("Waiting for calculate processes to complete....");

        while(count.get() < nThreads) {
            synchronized (resultList) {
                try {
                    resultList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("All process completed...");
        return resultList.stream().mapToInt(x -> x).sum();
    }

    static class ColumnSummator implements Runnable{

        private final List<Integer> nums;

        public ColumnSummator(List<Integer> nums) {
            this.nums = nums;
        }

        @Override
        public void run() {
            int sum = nums.stream().mapToInt(x -> x).sum();

            synchronized (resultList) {
                resultList.add(sum);
                count.getAndIncrement();
                resultList.notify();
            }
        }
    }
}
